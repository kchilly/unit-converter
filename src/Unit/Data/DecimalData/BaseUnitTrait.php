<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\Unit\Data\DecimalData;


use Csoft\UnitConverter\Unit\UnitInterface;

trait BaseUnitTrait
{
    /**
     * @inheritDoc
     */
    public function getBaseUnit(): UnitInterface
    {
        return new Bit();
    }
}
