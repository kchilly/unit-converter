<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\Unit\Data\DecimalData;


use Csoft\UnitConverter\Unit\UnitInterface;

class Kilobyte implements UnitInterface
{
    use BaseUnitTrait;

    /**
     * @inheritDoc
     */
    public function getBaseValue(): float
    {
        return 8 * 1000;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'kilobyte';
    }

    /**
     * @inheritDoc
     */
    public function getSymbol(): string
    {
        return 'kB';
    }

    /**
     * @inheritDoc
     */
    public function getAlternativeSymbols(): array
    {
        return [];
    }
}
