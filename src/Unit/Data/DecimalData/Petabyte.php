<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\Unit\Data\DecimalData;


use Csoft\UnitConverter\Unit\UnitInterface;

class Petabyte implements UnitInterface
{
    use BaseUnitTrait;

    /**
     * @inheritDoc
     */
    public function getBaseValue(): float
    {
        return 8 * (1000 ** 5);
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'petabyte';
    }

    /**
     * @inheritDoc
     */
    public function getSymbol(): string
    {
        return 'PB';
    }

    /**
     * @inheritDoc
     */
    public function getAlternativeSymbols(): array
    {
        return [];
    }
}
