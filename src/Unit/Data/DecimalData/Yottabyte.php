<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\Unit\Data\DecimalData;


use Csoft\UnitConverter\Unit\UnitInterface;

class Yottabyte implements UnitInterface
{
    use BaseUnitTrait;

    /**
     * @inheritDoc
     */
    public function getBaseValue(): float
    {
        return 8 * (1000 ** 8);
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'yottabyte';
    }

    /**
     * @inheritDoc
     */
    public function getSymbol(): string
    {
        return 'YB';
    }

    /**
     * @inheritDoc
     */
    public function getAlternativeSymbols(): array
    {
        return [];
    }
}
