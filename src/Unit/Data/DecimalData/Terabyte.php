<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\Unit\Data\DecimalData;


use Csoft\UnitConverter\Unit\UnitInterface;

class Terabyte implements UnitInterface
{
    use BaseUnitTrait;

    /**
     * @inheritDoc
     */
    public function getBaseValue(): float
    {
        return 8 * (1000 ** 4);
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'terabyte';
    }

    /**
     * @inheritDoc
     */
    public function getSymbol(): string
    {
        return 'TB';
    }

    /**
     * @inheritDoc
     */
    public function getAlternativeSymbols(): array
    {
        return [];
    }
}
