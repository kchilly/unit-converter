<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\Unit\Data\DecimalData;


use Csoft\UnitConverter\Unit\UnitInterface;

class Byte implements UnitInterface
{
    use BaseUnitTrait;

    /**
     * @inheritDoc
     */
    public function getBaseValue(): float
    {
        return 8;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'byte';
    }

    /**
     * @inheritDoc
     */
    public function getSymbol(): string
    {
        return 'B';
    }

    /**
     * @inheritDoc
     */
    public function getAlternativeSymbols(): array
    {
        return [];
    }
}
