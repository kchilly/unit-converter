<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\Unit\Data\DecimalData;


use Csoft\UnitConverter\Unit\UnitInterface;

class Zettabyte implements UnitInterface
{
    use BaseUnitTrait;

    /**
     * @inheritDoc
     */
    public function getBaseValue(): float
    {
        return 8 * (1000 ** 7);
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'zettabyte';
    }

    /**
     * @inheritDoc
     */
    public function getSymbol(): string
    {
        return 'ZB';
    }

    /**
     * @inheritDoc
     */
    public function getAlternativeSymbols(): array
    {
        return [];
    }
}
