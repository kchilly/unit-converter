<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\Unit\Data\DecimalData;


use Csoft\UnitConverter\Unit\UnitInterface;

class Bit implements UnitInterface
{
    use BaseUnitTrait;

    /**
     * @inheritDoc
     */
    public function getBaseValue(): float
    {
        return 1;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'bit';
    }

    /**
     * @inheritDoc
     */
    public function getSymbol(): string
    {
        return 'b';
    }

    /**
     * @inheritDoc
     */
    public function getAlternativeSymbols(): array
    {
        return [];
    }
}
