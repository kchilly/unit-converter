<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\Unit\Data\DecimalData;


use Csoft\UnitConverter\Unit\UnitInterface;

class Gigabyte implements UnitInterface
{
    use BaseUnitTrait;

    /**
     * @inheritDoc
     */
    public function getBaseValue(): float
    {
        return 8 * (1000 ** 3);
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'gigabyte';
    }

    /**
     * @inheritDoc
     */
    public function getSymbol(): string
    {
        return 'GB';
    }

    /**
     * @inheritDoc
     */
    public function getAlternativeSymbols(): array
    {
        return [];
    }
}
