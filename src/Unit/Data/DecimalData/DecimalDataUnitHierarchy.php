<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\Unit\Data\DecimalData;


use Csoft\UnitConverter\UnitHierarchy\AbstractUnitHierarchy;

class DecimalDataUnitHierarchy extends AbstractUnitHierarchy
{
    /**
     * @inheritDoc
     */
    protected function getBaseHierarchy(): array
    {
        return [
            Bit::class,
            Byte::class,
            Kilobyte::class,
            Megabyte::class,
            Gigabyte::class,
            Terabyte::class,
            Petabyte::class,
            Exabyte::class,
            Zettabyte::class,
            Yottabyte::class,
        ];
    }
}
