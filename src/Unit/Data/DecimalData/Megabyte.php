<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\Unit\Data\DecimalData;


use Csoft\UnitConverter\Unit\UnitInterface;

class Megabyte implements UnitInterface
{
    use BaseUnitTrait;

    /**
     * @inheritDoc
     */
    public function getBaseValue(): float
    {
        return 8 * (1000 ** 2);
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'megabyte';
    }

    /**
     * @inheritDoc
     */
    public function getSymbol(): string
    {
        return 'MB';
    }

    /**
     * @inheritDoc
     */
    public function getAlternativeSymbols(): array
    {
        return [];
    }
}
