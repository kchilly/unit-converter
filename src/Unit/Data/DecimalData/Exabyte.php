<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\Unit\Data\DecimalData;


use Csoft\UnitConverter\Unit\UnitInterface;

class Exabyte implements UnitInterface
{
    use BaseUnitTrait;

    /**
     * @inheritDoc
     */
    public function getBaseValue(): float
    {
        return 8 * (1000 ** 6);
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'exabyte';
    }

    /**
     * @inheritDoc
     */
    public function getSymbol(): string
    {
        return 'EB';
    }

    /**
     * @inheritDoc
     */
    public function getAlternativeSymbols(): array
    {
        return [];
    }
}
