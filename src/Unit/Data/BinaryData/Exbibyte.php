<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\Unit\Data\BinaryData;


use Csoft\UnitConverter\Unit\UnitInterface;

class Exbibyte implements UnitInterface
{
    use BaseUnitTrait;

    /**
     * @inheritDoc
     */
    public function getBaseValue(): float
    {
        return 8 * (1024 ** 6);
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'exbibyte';
    }

    /**
     * @inheritDoc
     */
    public function getSymbol(): string
    {
        return 'EiB';
    }

    /**
     * @inheritDoc
     */
    public function getAlternativeSymbols(): array
    {
        return [];
    }
}
