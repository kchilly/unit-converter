<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\Unit\Data\BinaryData;


use Csoft\UnitConverter\UnitHierarchy\AbstractUnitHierarchy;

class BinaryDataUnitHierarchy extends AbstractUnitHierarchy
{
    /**
     * @inheritDoc
     */
    protected function getBaseHierarchy(): array
    {
        return [
            Bit::class,
            Byte::class,
            Kibibyte::class,
            Mebibyte::class,
            Gibibyte::class,
            Tebibyte::class,
            Pebibyte::class,
            Exbibyte::class,
            Zebibyte::class,
            Yobibyte::class,
        ];
    }
}
