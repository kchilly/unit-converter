<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\Unit\Data\BinaryData;


use Csoft\UnitConverter\Unit\UnitInterface;

class Pebibyte implements UnitInterface
{
    use BaseUnitTrait;

    /**
     * @inheritDoc
     */
    public function getBaseValue(): float
    {
        return 8 * (1024 ** 5);
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'pebibyte';
    }

    /**
     * @inheritDoc
     */
    public function getSymbol(): string
    {
        return 'PiB';
    }

    /**
     * @inheritDoc
     */
    public function getAlternativeSymbols(): array
    {
        return [];
    }
}
