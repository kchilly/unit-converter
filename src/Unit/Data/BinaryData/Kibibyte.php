<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\Unit\Data\BinaryData;


use Csoft\UnitConverter\Unit\AlternativeName;
use Csoft\UnitConverter\Unit\UnitInterface;

class Kibibyte implements UnitInterface
{
    use BaseUnitTrait;

    /**
     * @inheritDoc
     */
    public function getBaseValue(): float
    {
        return 8 * 1024;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'kibibyte';
    }

    /**
     * @inheritDoc
     */
    public function getSymbol(): string
    {
        return 'KiB';
    }

    /**
     * @inheritDoc
     */
    public function getAlternativeSymbols(): array
    {
        return [
            new AlternativeName('KB', 'kilobyte'), // JEDEC
        ];
    }
}
