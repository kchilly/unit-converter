<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\Unit\Data\BinaryData;


use Csoft\UnitConverter\Unit\UnitInterface;

class Zebibyte implements UnitInterface
{
    use BaseUnitTrait;

    /**
     * @inheritDoc
     */
    public function getBaseValue(): float
    {
        return 8 * (1024 ** 7);
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'zebibyte';
    }

    /**
     * @inheritDoc
     */
    public function getSymbol(): string
    {
        return 'ZiB';
    }

    /**
     * @inheritDoc
     */
    public function getAlternativeSymbols(): array
    {
        return [];
    }
}
