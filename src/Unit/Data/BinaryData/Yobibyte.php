<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\Unit\Data\BinaryData;


use Csoft\UnitConverter\Unit\UnitInterface;

class Yobibyte implements UnitInterface
{
    use BaseUnitTrait;

    /**
     * @inheritDoc
     */
    public function getBaseValue(): float
    {
        return 8 * (1024 ** 8);
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'yobibyte';
    }

    /**
     * @inheritDoc
     */
    public function getSymbol(): string
    {
        return 'YiB';
    }

    /**
     * @inheritDoc
     */
    public function getAlternativeSymbols(): array
    {
        return [];
    }
}
