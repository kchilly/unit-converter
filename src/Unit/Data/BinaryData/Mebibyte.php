<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\Unit\Data\BinaryData;


use Csoft\UnitConverter\Unit\AlternativeName;
use Csoft\UnitConverter\Unit\UnitInterface;

class Mebibyte implements UnitInterface
{
    use BaseUnitTrait;

    /**
     * @inheritDoc
     */
    public function getBaseValue(): float
    {
        return 8 * (1024 ** 2);
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'mebibyte';
    }

    /**
     * @inheritDoc
     */
    public function getSymbol(): string
    {
        return 'MiB';
    }

    /**
     * @inheritDoc
     */
    public function getAlternativeSymbols(): array
    {
        return [
            new AlternativeName('MB', 'megabyte'), // JEDEC
        ];
    }
}
