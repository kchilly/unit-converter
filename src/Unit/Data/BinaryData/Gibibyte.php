<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\Unit\Data\BinaryData;


use Csoft\UnitConverter\Unit\AlternativeName;
use Csoft\UnitConverter\Unit\UnitInterface;

class Gibibyte implements UnitInterface
{
    use BaseUnitTrait;

    /**
     * @inheritDoc
     */
    public function getBaseValue(): float
    {
        return 8 * (1024 ** 3);
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'gibibyte';
    }

    /**
     * @inheritDoc
     */
    public function getSymbol(): string
    {
        return 'GiB';
    }

    /**
     * @inheritDoc
     */
    public function getAlternativeSymbols(): array
    {
        return [
            new AlternativeName('GB', 'gigabyte'), // JEDEC
        ];
    }
}
