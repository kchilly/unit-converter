<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\Unit\Data\BinaryData;


use Csoft\UnitConverter\Unit\UnitInterface;

class Tebibyte implements UnitInterface
{
    use BaseUnitTrait;

    /**
     * @inheritDoc
     */
    public function getBaseValue(): float
    {
        return 8 * (1024 ** 4);
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return 'tebibyte';
    }

    /**
     * @inheritDoc
     */
    public function getSymbol(): string
    {
        return 'TiB';
    }

    /**
     * @inheritDoc
     */
    public function getAlternativeSymbols(): array
    {
        return [];
    }
}
