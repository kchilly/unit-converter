<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\Unit;


interface UnitInterface
{
    /**
     * Returns the name of the unit.
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Returns the symbol of the unit.
     *
     * @return string
     */
    public function getSymbol(): string;

    /**
     * Returns the alternative names of the unit.
     *
     * @return AlternativeName[]
     */
    public function getAlternativeSymbols(): array;

    /**
     * Returns the value in the base unit.
     *
     * @return float
     */
    public function getBaseValue(): float;

    /**
     * Returns the base unit for the base value.
     *
     * @return UnitInterface
     */
    public function getBaseUnit(): UnitInterface;
}
