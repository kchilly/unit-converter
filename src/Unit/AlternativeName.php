<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\Unit;


class AlternativeName
{
    private string $symbol;
    private string $name;

    /**
     * AlternativeName constructor.
     *
     * @param string $symbol
     * @param string $name
     */
    public function __construct(string $symbol, string $name)
    {
        $this->symbol = $symbol;
        $this->name = $name;
    }

    /**
     * Returns the alternative symbol.
     *
     * @return string
     */
    public function getSymbol(): string
    {
        return $this->symbol;
    }

    /**
     * Returns the alternative name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
