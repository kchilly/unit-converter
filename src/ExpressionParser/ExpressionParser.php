<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\ExpressionParser;


use Csoft\UnitConverter\Quantity\QuantityInterface;
use InvalidArgumentException;

class ExpressionParser implements ExpressionParserInterface
{
    /**
     * Returns the quantities parsed from text.
     *
     * @param string $expression
     * @param array $unitFqns
     *
     * @return QuantityInterface[]
     */
    public function getQuantitiesFromExpression(string $expression, array $unitFqns): array
    {
        $parsedContent = $this->parse($expression);
        if (empty($parsedContent)) {
            throw new InvalidArgumentException(
                'The requested expression doesn not contain any valid quantity!'
            );
        }

        return $this->parsedContentToQuantities(
            $parsedContent['numbers'],
            $parsedContent['symbols'],
            $unitFqns
        );
    }

    /**
     * Returns the quantity instances built from the given numbers and symbols.
     *
     * @param array $numbers
     * @param array $symbols
     * @param array $unitFqns
     *
     * @return array
     */
    private function parsedContentToQuantities(array $numbers, array $symbols, array $unitFqns): array
    {
        $unitFqnMap = $this->getUnitFqnMap($unitFqns);

        $quantities = [];
        foreach ($numbers as $index => $number) {
            $number = (float)strtr($number, [',' => '.']);
            $symbol = $symbols[$index];
            if (isset($unitFqnMap[$symbol]) === false) {
                throw new InvalidArgumentException(
                    'The expression contains at least one invalid unit symbol!'
                );
            }
            $quantities[] = new $unitFqnMap[$symbol]($number);
        }

        return $quantities;
    }

    /**
     * Returns the quantities parsed from text.
     *
     * @param string $expression
     *
     * @return array
     */
    private function parse(string $expression): array
    {
        if (preg_match_all(
            '#(?<numbers>[0-9.,]+)([ ]+)?(?<symbols>[a-z]+)([ ]+)?(?<operators>[+-/*])?#i',
            $expression,
            $matches
        )) {
            return [
                'numbers' => $matches['numbers'],
                'symbols' => $matches['symbols'],
                'operators' => $matches['operators'],
            ];
        }

        return [];
    }

    /**
     * Returns the unit-fqn map.
     *
     * @param array $unitFqns
     *
     * @return array
     */
    private function getUnitFqnMap(array $unitFqns): array
    {
        $unitFqnMap = [];
        foreach ($unitFqns as $unitFqn) {
            /** @var QuantityInterface $quantity */
            $quantity = new $unitFqn(1);
            $unitFqnMap[$quantity->getSymbol()] = $unitFqn;
            foreach ($quantity->getAlternativeSymbols() as $alternativeSymbol) {
                $unitFqnMap[$alternativeSymbol->getSymbol()] = $unitFqn;
            }
        }

        return $unitFqnMap;
    }
}
