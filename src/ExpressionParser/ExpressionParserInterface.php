<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\ExpressionParser;


use Csoft\UnitConverter\Quantity\QuantityInterface;
use InvalidArgumentException;

interface ExpressionParserInterface
{
    /**
     * Returns the quantities parsed from text.
     *
     * @param string $expression
     * @param array $unitFqns
     *
     * @return QuantityInterface[]
     *
     * @throws InvalidArgumentException
     */
    public function getQuantitiesFromExpression(string $expression, array $unitFqns): array;
}
