<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\QuantityFactory;


use Csoft\UnitConverter\ExpressionParser\ExpressionParser;
use Csoft\UnitConverter\Unit\Data\BinaryData\BinaryDataUnitHierarchy;

class BinaryDataQuantityFactory extends AbstractQuantityFactory
{
    /**
     * @inheritDoc
     */
    public static function getInstance(): AbstractQuantityFactory
    {
        return new static(new BinaryDataUnitHierarchy(), new ExpressionParser());
    }
}
