<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\QuantityFactory;


use Csoft\UnitConverter\Quantity\QuantityInterface;

interface QuantityFactoryInterface
{
    /**
     * Converts the given quantity to a human readable unit and returns a new quantity based on this unit.
     *
     * @param QuantityInterface $quantity
     * @param array $skipUnitFqns
     *
     * @return QuantityInterface
     */
    public function createHumanReadableQuantity(
        QuantityInterface $quantity,
        array $skipUnitFqns = []
    ): QuantityInterface;

    /**
     * Create a quantity instance based on the given text.
     *
     * @param string $text
     * @param array $skipUnitFqns
     *
     * @return QuantityInterface[]
     */
    public function createFromText(string $text, array $skipUnitFqns = []): array;
}
