<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\QuantityFactory;


use Csoft\UnitConverter\ExpressionParser\ExpressionParserInterface;
use Csoft\UnitConverter\Quantity\QuantityInterface;
use Csoft\UnitConverter\UnitHierarchy\UnitHierarchyInterface;
use InvalidArgumentException;

abstract class AbstractQuantityFactory implements QuantityFactoryInterface
{
    /**
     * @var UnitHierarchyInterface
     */
    private UnitHierarchyInterface $unitHierarchy;
    /**
     * @var ExpressionParserInterface
     */
    private ExpressionParserInterface $expressionParser;

    /**
     * AbstractQuantityFactory constructor.
     *
     * @param UnitHierarchyInterface $unitHierarchy
     * @param ExpressionParserInterface $expressionParser
     */
    public function __construct(
        UnitHierarchyInterface $unitHierarchy,
        ExpressionParserInterface $expressionParser
    ) {
        $this->unitHierarchy = $unitHierarchy;
        $this->expressionParser = $expressionParser;
    }

    /**
     * Returns a Factory instance.
     *
     * @return $this
     */
    abstract public static function getInstance(): self;

    /**
     * @inheritDoc
     */
    public function createFromText(string $text, array $skipUnitFqns = []): array
    {
        return $this->expressionParser->getQuantitiesFromExpression(
            $text,
            $this->unitHierarchy->getHierarchy($skipUnitFqns)
        );
    }

    /**
     * @inheritDoc
     */
    public function createHumanReadableQuantity(
        QuantityInterface $quantity,
        array $skipUnitFqns = []
    ): QuantityInterface
    {
        $reverseHierarchy = $this->unitHierarchy->getReverseHierarchy($skipUnitFqns);
        if (empty($reverseHierarchy)) {
            throw new InvalidArgumentException(
                'There aren\'t any unit allowed to generate human readable quantity!'
            );
        }

        foreach ($reverseHierarchy as $unitClassFqn) {
            /** @var QuantityInterface $currentQuantity */
            $currentQuantity = call_user_func([$unitClassFqn, 'createFromQuantity'], $quantity);
            if ($currentQuantity->getBaseValue() >= $currentQuantity::getBaseMultiplier()) {
                return $currentQuantity;
            }
        }

        return $currentQuantity;
    }
}
