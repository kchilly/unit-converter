<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\UnitHierarchy;


interface UnitHierarchyInterface
{
    /**
     * Returns the unit hierarchy optionally skips the wanted units.
     *
     * @param string[] $skipUnitFqns
     *
     * @return string[]
     */
    public function getHierarchy(array $skipUnitFqns = []): array;

    /**
     * Returns the unit hierarchy reversed optionally skips the wanted units.
     *
     * @param string[] $skipUnitFqns
     *
     * @return string[]
     */
    public function getReverseHierarchy(array $skipUnitFqns = []): array;
}
