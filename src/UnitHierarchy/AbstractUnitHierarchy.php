<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\UnitHierarchy;


abstract class AbstractUnitHierarchy implements UnitHierarchyInterface
{
    /**
     * @inheritDoc
     */
    public function getHierarchy(array $skipUnitFqns = []): array
    {
        $unitHierarchyFqns = $this->getBaseHierarchy();
        foreach ($unitHierarchyFqns as $index => $unitHierarchyFqn) {
            if (in_array($unitHierarchyFqn, $skipUnitFqns, true)) {
                unset($unitHierarchyFqns[$index]);
            }
        }

        return $unitHierarchyFqns;
    }

    /**
     * @inheritDoc
     */
    public function getReverseHierarchy(array $skipUnitFqns = []): array
    {
        return array_reverse(
            $this->getHierarchy($skipUnitFqns)
        );
    }

    /**
     * Returns the base unit hierarchy.
     *
     * @return string[]
     */
    abstract protected function getBaseHierarchy(): array;
}
