<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\Quantity;


use Csoft\UnitConverter\Unit\UnitInterface;

interface QuantityInterface
{
    /**
     * Returns the quantity.
     *
     * @return float
     */
    public function getValue(): float;

    /**
     * Returns the unit.
     *
     * @return UnitInterface
     */
    public function getUnit(): UnitInterface;

    /**
     * Returns the quantity instance in the requested unit.
     *
     * @param UnitInterface $unit
     *
     * @return $this
     */
    public function convertTo(UnitInterface $unit): self;

    /**
     * Returns the quantity instance in the base unit.
     *
     * @return $this
     */
    public function getQuantityInBaseUnit(): self;

    /**
     * Returns the quantity representation as text.
     *
     * @return string
     */
    public function __toString(): string;
}
