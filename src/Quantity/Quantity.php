<?php

declare(strict_types=1);


namespace Csoft\UnitConverter\Quantity;


use Csoft\UnitConverter\Unit\UnitInterface;

class Quantity implements QuantityInterface
{
    private float $quantity;
    private UnitInterface $unit;

    /**
     * Quantity constructor.
     *
     * @param float $quantity
     * @param UnitInterface $unit
     */
    public function __construct(float $quantity, UnitInterface $unit)
    {
        $this->quantity = $quantity;
        $this->unit = $unit;
    }

    /**
     * @inheritDoc
     */
    public function getValue(): float
    {
        return $this->quantity;
    }

    /**
     * @inheritDoc
     */
    public function getUnit(): UnitInterface
    {
        return $this->unit;
    }

    /**
     * @inheritDoc
     */
    public function convertTo(UnitInterface $unit): self
    {
        return new static(
            $this->quantity * ($this->unit->getBaseValue()/$unit->getBaseValue()),
            $unit
        );
    }

    /**
     * @inheritDoc
     */
    public function getQuantityInBaseUnit(): self
    {
        return $this->convertTo($this->unit->getBaseUnit());
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return sprintf(
            '%s%s',
            $this->quantity,
            $this->unit->getSymbol(),
        );
    }
}
