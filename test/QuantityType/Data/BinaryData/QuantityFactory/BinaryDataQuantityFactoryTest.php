<?php

declare(strict_types=1);


namespace Csoft\UnitConverterTest\QuantityType\Data\BinaryData\QuantityFactory;


use Csoft\UnitConverter\Quantity\QuantityInterface;
use Csoft\UnitConverter\QuantityFactory\BinaryDataQuantityFactory;
use Csoft\UnitConverter\Unit\Data\BinaryData\Bit;
use Csoft\UnitConverter\Unit\Data\BinaryData\Byte;
use Csoft\UnitConverter\Unit\Data\BinaryData\Exbibyte;
use Csoft\UnitConverter\Unit\Data\BinaryData\Gibibyte;
use Csoft\UnitConverter\Unit\Data\BinaryData\Kibibyte;
use Csoft\UnitConverter\Unit\Data\BinaryData\Mebibyte;
use Csoft\UnitConverter\Unit\Data\BinaryData\Pebibyte;
use Csoft\UnitConverter\Unit\Data\BinaryData\Tebibyte;
use Csoft\UnitConverter\Unit\Data\BinaryData\Yobibyte;
use Csoft\UnitConverter\Unit\Data\BinaryData\Zebibyte;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class BinaryDataQuantityFactoryTest extends TestCase
{
    /**
     * @param string $exception
     * @param string $exceptionMessage
     * @param string $text
     *
     * @dataProvider provideTestCasesForTestCreateFromTextFailure
     */
    public function testCreateFromTextFailure(string $exception, string $exceptionMessage, string $text): void
    {
        $this->expectException($exception);
        $this->expectExceptionMessage($exceptionMessage);

        BinaryDataQuantityFactory::getInstance()->createFromText($text);
    }

    public function provideTestCasesForTestCreateFromTextFailure(): array
    {
        return [
            [
                InvalidArgumentException::class,
                'The expression contains at least one invalid unit symbol!',
                '   15  kb  ',
            ],
            [
                InvalidArgumentException::class,
                'The expression contains at least one invalid unit symbol!',
                ' 20MiB 15 MB   15  kb  ',
            ],
            [
                InvalidArgumentException::class,
                'The requested expression doesn not contain any valid quantity!',
                ' 20  ',
            ],
        ];
    }

    /**
     * @param string $expected
     * @param string $text
     *
     * @dataProvider provideTestCasesForTestCreateFromTextSuccess
     */
    public function testCreateFromTextSuccess(string $expected, string $text): void
    {
        self::assertEquals(
            $expected,
            implode('|', BinaryDataQuantityFactory::getInstance()->createFromText($text))
        );
    }

    public function provideTestCasesForTestCreateFromTextSuccess(): array
    {
        return [
            [
                '15KiB',
                '15KiB',
            ],
            [
                '15KiB',
                '15 KiB',
            ],
            [
                '15KiB',
                '    15    KiB    ',
            ],
            [
                '15.4KiB',
                '    15.4    KiB    ',
            ],
            [
                '15.4KiB',
                '    15,4    KiB    ',
            ],
            [
                '5MiB|15.4KiB|72.4B',
                ' 5 MiB   15,4    KiB    72.4   B',
            ],
            [
                '5KiB|15.4MiB|72.4GiB',
                ' 5 KB   15,4    MB    72.4   GB',
            ],
        ];
    }

    /**
     * @param string $expected
     * @param QuantityInterface $quantityWithUnit
     * @param array $skipUnitFqns
     *
     * @dataProvider provideTestCasesForTestCreateHumanReadableQuantity
     */
    public function testCreateHumanReadableQuantityWithUnit(string $expected, QuantityInterface $quantityWithUnit, array $skipUnitFqns = []): void
    {
        self::assertEquals(
            $expected,
            (string)BinaryDataQuantityFactory::getInstance()->createHumanReadableQuantity($quantityWithUnit, $skipUnitFqns)
        );
    }

    public function provideTestCasesForTestCreateHumanReadableQuantity(): array
    {
        return [
            [
                '1536YiB',
                new Yobibyte(1536),
            ],
            [
                '1572864ZiB',
                new Yobibyte(1536),
                [Yobibyte::class],
            ],
            [
                '1.5YiB',
                new Zebibyte(1536),
            ],
            [
                '1.5ZiB',
                new Exbibyte(1536),
            ],
            [
                '1.5EiB',
                new Pebibyte(1536),
            ],
            [
                '1.5PiB',
                new Tebibyte(1536),
            ],
            [
                '1.5TiB',
                new Gibibyte(1536),
            ],
            [
                '1.5GiB',
                new Mebibyte(1536),
            ],
            [
                '131072KiB',
                new Mebibyte(128),
                [
                    Gibibyte::class,
                    Mebibyte::class,
                ],
            ],
            [
                '1MiB',
                new Kibibyte(1024),
            ],
            [
                '1.5KiB',
                new Byte(1536),
            ],
            [
                '2B',
                new Bit(16),
            ],
            [
                '0.5b',
                new Bit(0.5),
            ],
            [
                '1536YiB',
                Mebibyte::createFromQuantity(new Yobibyte(1536)),
            ],
        ];
    }

    public function testCreateHumanReadableQuantityWithUnitNoUnits(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('There aren\'t any unit allowed to generate human readable quantity!');

        BinaryDataQuantityFactory::getInstance()->createHumanReadableQuantity(
            new Mebibyte(5),
            [
                Bit::class,
                Byte::class,
                Kibibyte::class,
                Mebibyte::class,
                Gibibyte::class,
                Tebibyte::class,
                Pebibyte::class,
                Exbibyte::class,
                Zebibyte::class,
                Yobibyte::class,
            ]
        );
    }

    /**
     * @param float $expectedValue
     * @param string $expectedName
     * @param string $text
     *
     * @dataProvider provideTestCasesForTestCreateFromTextSuccessName
     */
    public function testCreateFromTextSuccessName(float $expectedValue, string $expectedName, string $text): void
    {
        $quantityWithUnit = BinaryDataQuantityFactory::getInstance()->createFromText($text)[0];
        self::assertEquals(
            $expectedValue,
            $quantityWithUnit->getValue()
        );
        self::assertEquals(
            $expectedName,
            $quantityWithUnit->getName()
        );
    }

    public function provideTestCasesForTestCreateFromTextSuccessName(): array
    {
        return [
            [
                5.0,
                'bit',
                '5b',
            ],
            [
                6.0,
                'byte',
                '6B',
            ],
            [
                7.0,
                'kibibyte',
                '7KiB',
            ],
            [
                8.0,
                'mebibyte',
                '8MiB',
            ],
            [
                9.0,
                'gibibyte',
                '9GiB',
            ],
            [
                1.0,
                'tebibyte',
                '1TiB',
            ],
            [
                2.0,
                'pebibyte',
                '2PiB',
            ],
            [
                3.0,
                'exbibyte',
                '3EiB',
            ],
            [
                4.0,
                'zebibyte',
                '4ZiB',
            ],
            [
                5.0,
                'yobibyte',
                '5YiB',
            ],
        ];
    }

    /**
     * @param string $expectedName
     * @param string $text
     *
     * @dataProvider provideTestCasesForTestCreateFromTextSuccessAlternativeName
     */
    public function testCreateFromTextSuccessAlternativeName(string $expectedName, string $text): void
    {
        $quantityWithUnit = BinaryDataQuantityFactory::getInstance()->createFromText($text)[0];
        self::assertEquals(
            $expectedName,
            $quantityWithUnit->getAlternativeSymbols()[0]->getName() ?? $quantityWithUnit->getName()
        );
    }

    public function provideTestCasesForTestCreateFromTextSuccessAlternativeName(): array
    {
        return [
            [
                'kilobyte',
                '7KB',
            ],
            [
                'megabyte',
                '8MB',
            ],
            [
                'gigabyte',
                '9GB',
            ],
        ];
    }
}
