<?php

declare(strict_types=1);


namespace Csoft\UnitConverterTest\QuantityType\Data\BinaryData;


use Csoft\UnitConverter\Unit\Data\BinaryData\Bit;
use Csoft\UnitConverter\Unit\Data\BinaryData\Byte;
use Csoft\UnitConverter\Unit\Data\BinaryData\Exbibyte;
use Csoft\UnitConverter\Unit\Data\BinaryData\Gibibyte;
use Csoft\UnitConverter\Unit\Data\BinaryData\Kibibyte;
use Csoft\UnitConverter\Unit\Data\BinaryData\Mebibyte;
use Csoft\UnitConverter\Unit\Data\BinaryData\Pebibyte;
use Csoft\UnitConverter\Unit\Data\BinaryData\Tebibyte;
use Csoft\UnitConverter\Unit\Data\BinaryData\Yobibyte;
use Csoft\UnitConverter\Unit\Data\BinaryData\Zebibyte;

trait UnitConversionTestCaseProviderTrait
{
    public function provideTestCasesForTestUnitConversion(): array
    {
        return [
            'fromBit' => [
                7,
                new Bit(7),
            ],
            'fromByte' => [
                88,
                new Byte(11),
            ],
            'fromKibibyte' => [
                8 * 10 * 1024,
                new Kibibyte(10),
            ],
            'fromMebibyte' => [
                8 * 5 * (1024 ** 2),
                new Mebibyte(5),
            ],
            'fromGibibyte' => [
                8 * 7 * (1024 ** 3),
                new Gibibyte(7),
            ],
            'fromTebibyte' => [
                8 * 11 * (1024 ** 4),
                new Tebibyte(11),
            ],
            'fromPebibyte' => [
                8 * 13 * (1024 ** 5),
                new Pebibyte(13),
            ],
            'fromExbibyte' => [
                8 * 15 * (1024 ** 6),
                new Exbibyte(15),
            ],
            'fromZebibyte' => [
                8 * 17 * (1024 ** 7),
                new Zebibyte(17),
            ],
            'fromYobibyte' => [
                8 * 21 * (1024 ** 8),
                new Yobibyte(21),
            ],
        ];
    }
}
