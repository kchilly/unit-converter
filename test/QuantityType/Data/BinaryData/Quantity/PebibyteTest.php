<?php

declare(strict_types=1);


namespace Csoft\UnitConverterTest\QuantityType\Data\BinaryData\Quantity;


use Csoft\UnitConverter\Quantity\QuantityInterface;
use Csoft\UnitConverter\Unit\Data\BinaryData\Pebibyte;
use Csoft\UnitConverterTest\QuantityType\Data\BinaryData\UnitConversionTestCaseProviderTrait;
use PHPUnit\Framework\TestCase;

class PebibyteTest extends TestCase
{
    use UnitConversionTestCaseProviderTrait;

    public function testToString(): void
    {
        self::assertEquals(
            '512PiB',
            (string)(new Pebibyte(512))
        );
    }

    /**
     * @param float $expected
     * @param float $quantity
     *
     * @dataProvider provideTestCasesForTestQuantityCalculation
     */
    public function testQuantityCalculation(float $expected, float $quantity): void
    {
        $quantityWithUnit = new Pebibyte($quantity);

        self::assertEquals(
            $expected,
            $quantityWithUnit->getBaseValue()
        );
    }

    public function provideTestCasesForTestQuantityCalculation(): array
    {
        return [
            'quantity' => [
                5 * 8 * (1024 ** 5),
                5,
            ],
        ];
    }

    /**
     * @param float $expected
     * @param QuantityInterface $quantityWithUnit
     *
     * @dataProvider provideTestCasesForTestUnitConversion
     */
    public function testUnitConversion(float $expected, QuantityInterface $quantityWithUnit): void
    {
        self::assertEquals(
            $expected,
            Pebibyte::createFromQuantity($quantityWithUnit)->getBaseValue()
        );
    }
}
