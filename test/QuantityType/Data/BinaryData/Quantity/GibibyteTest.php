<?php

declare(strict_types=1);


namespace Csoft\UnitConverterTest\QuantityType\Data\BinaryData\Quantity;


use Csoft\UnitConverter\Quantity\QuantityInterface;
use Csoft\UnitConverter\Unit\Data\BinaryData\Gibibyte;
use Csoft\UnitConverterTest\QuantityType\Data\BinaryData\UnitConversionTestCaseProviderTrait;
use PHPUnit\Framework\TestCase;

class GibibyteTest extends TestCase
{
    use UnitConversionTestCaseProviderTrait;

    public function testToString(): void
    {
        self::assertEquals(
            '512GiB',
            (string)(new Gibibyte(512))
        );
    }

    /**
     * @param float $expected
     * @param float $quantity
     *
     * @dataProvider provideTestCasesForTestQuantityCalculation
     */
    public function testQuantityCalculation(float $expected, float $quantity): void
    {
        $quantityWithUnit = new Gibibyte($quantity);

        self::assertEquals(
            $expected,
            $quantityWithUnit->getBaseValue()
        );
    }

    public function provideTestCasesForTestQuantityCalculation(): array
    {
        return [
            'quantity' => [
                5 * 8 * (1024 ** 3),
                5,
            ],
        ];
    }

    /**
     * @param float $expected
     * @param QuantityInterface $quantityWithUnit
     *
     * @dataProvider provideTestCasesForTestUnitConversion
     */
    public function testUnitConversion(float $expected, QuantityInterface $quantityWithUnit): void
    {
        self::assertEquals(
            $expected,
            Gibibyte::createFromQuantity($quantityWithUnit)->getBaseValue()
        );
    }
}
