<?php

declare(strict_types=1);


namespace Csoft\UnitConverterTest\QuantityType\Data\BinaryData\Quantity;


use Csoft\UnitConverter\Quantity\QuantityInterface;
use Csoft\UnitConverter\Unit\Data\BinaryData\Byte;
use Csoft\UnitConverterTest\QuantityType\Data\BinaryData\UnitConversionTestCaseProviderTrait;
use PHPUnit\Framework\TestCase;

class ByteTest extends TestCase
{
    use UnitConversionTestCaseProviderTrait;

    public function testToString(): void
    {
        self::assertEquals(
            '512B',
            (string)(new Byte(512))
        );
    }

    /**
     * @param float $expected
     * @param float $quantity
     *
     * @dataProvider provideTestCasesForTestQuantityCalculation
     */
    public function testQuantityCalculation(float $expected, float $quantity): void
    {
        $quantityWithUnit = new Byte($quantity);

        self::assertEquals(
            $expected,
            $quantityWithUnit->getBaseValue()
        );
    }

    public function provideTestCasesForTestQuantityCalculation(): array
    {
        return [
            'quantity' => [
                5 * 8,
                5,
            ],
        ];
    }

    /**
     * @param float $expected
     * @param QuantityInterface $quantityWithUnit
     *
     * @dataProvider provideTestCasesForTestUnitConversion
     */
    public function testUnitConversion(float $expected, QuantityInterface $quantityWithUnit): void
    {
        self::assertEquals(
            $expected,
            Byte::createFromQuantity($quantityWithUnit)->getBaseValue()
        );
    }
}
