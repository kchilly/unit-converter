<?php

declare(strict_types=1);


namespace Csoft\UnitConverterTest\QuantityType\Data\BinaryData\Quantity;


use Csoft\UnitConverter\Quantity\QuantityInterface;
use Csoft\UnitConverter\Unit\Data\BinaryData\Bit;
use Csoft\UnitConverterTest\QuantityType\Data\BinaryData\UnitConversionTestCaseProviderTrait;
use PHPUnit\Framework\TestCase;

class BitTest extends TestCase
{
    use UnitConversionTestCaseProviderTrait;

    public function testToString(): void
    {
        self::assertEquals(
            '512b',
            (string)(new Bit(512))
        );
    }

    /**
     * @param float $expected
     * @param float $quantity
     *
     * @dataProvider provideTestCasesForTestQuantityCalculation
     */
    public function testQuantityCalculation(float $expected, float $quantity): void
    {
        $quantityWithUnit = new Bit($quantity);

        self::assertEquals(
            $expected,
            $quantityWithUnit->getBaseValue()
        );
    }

    public function provideTestCasesForTestQuantityCalculation(): array
    {
        return [
            'quantity' => [
                5,
                5,
            ],
        ];
    }

    /**
     * @param float $expected
     * @param QuantityInterface $quantityWithUnit
     *
     * @dataProvider provideTestCasesForTestUnitConversion
     */
    public function testUnitConversion(float $expected, QuantityInterface $quantityWithUnit): void
    {
        self::assertEquals(
            $expected,
            Bit::createFromQuantity($quantityWithUnit)->getBaseValue()
        );
    }
}
